package com.prueba.meli;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType; 
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner; 
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prueba.meli.entidad.ResultadoADN;
import com.prueba.meli.entidad.SecuenciaADN;
import com.prueba.meli.entidad.EstadoADN; 
 
//@DataMongoTest
//@ExtendWith(SpringExtension.class)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PruebaMutanteMeLiApplicationTests {

	@Autowired
	private WebApplicationContext appContext;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.appContext).build();
	}

	@Test
	public void contextLoads() {
	}

	 
	@Test
	public void test01InicioEstadisticas() throws Exception {
		
		ResultActions result = mockMvc.perform(
				get("/stats/")
					.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.count_mutant_dna").value(0))
				.andExpect(jsonPath("$.count_human_dna").value(0))
				.andExpect(jsonPath("$.ratio").value(0));
		
		byte[] content = result.andReturn().getResponse().getContentAsByteArray();

		EstadoADN dnaResult = new ObjectMapper().readValue(content, EstadoADN.class);
		System.out.println(dnaResult);
		Assert.assertTrue(dnaResult.getCantidadHumanos() == 0L);
		Assert.assertTrue(dnaResult.getCantidadMutantes() == 0L);
		Assert.assertTrue(dnaResult.getTotal() == 0L);
		Assert.assertTrue(dnaResult.getRatio().equals(BigDecimal.ZERO));
		
		
	}

	/**
	 * Test secuencias humanas
	 * 
	 * @throws Exception
	 */
	@Test
	public void test02TestHumanDNA() throws Exception {
		
		String request = "{\n"+ 
				"\"dna\": " +
				"        [\"ACCTATC\",\n" + 
				"         \"CTCACTT\",\n" + 
				"         \"ACGCTAT\",\n" + 
				"         \"ACCTACC\",\n" + 
				"         \"CAATTCC\",\n" + 
				"         \"CACCAAT\",\n" + 
				"         \"CAACAAT\"" +
				"        ]\n" +
				"}"; 
		mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isForbidden())
			;
		
	}

	/**
	 * Test one DNA only
	 * 
	 * @throws Exception
	 */
	@Test
	public void test02aTestOnlyOneDNA() throws Exception {
		test02TestHumanDNA();
		test02TestHumanDNA();
		test02TestHumanDNA();
		
		mockMvc.perform(
				get("/stats/")
					.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.count_mutant_dna").value(0))
				.andExpect(jsonPath("$.count_human_dna").value(1))
				.andExpect(jsonPath("$.ratio").value(0));
		
	}

	/**
	 * Test - Mutant/Human - Status with one human
	 * 
	 * @throws Exception
	 */
	@Test
	public void test03StatsOneHuman() throws Exception {
		
		mockMvc.perform(
				get("/stats/")
					.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.count_mutant_dna").value(0))
				.andExpect(jsonPath("$.count_human_dna").value(1))
				.andExpect(jsonPath("$.ratio").value(0));
		
		
	}

	/**
	 * Test - Mutant - Test Diagonal UP - find in last position valid
	 * 
	 * @throws Exception
	 */
	@Test
	public void test04MutantDiagnonalUPLastPositionValid() throws Exception {
		
		String request = "{\n" + 
				"	\"dna\": [\"AACCCC\",\n" + 
				"	        \"CTCAGC\",\n" + 
				"	        \"ACCAAA\",\n" + 
				"	        \"ACAAAC\",\n" + 
				"	        \"CAAATC\",\n" + 
				"	        \"CAACAA\"]\n" + 
				"}\n" + 
				""; 
		mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isOk());
		
	}

	/**
	 * Test - Mutant - Test Diagonal UP - find at row 0
	 * 
	 * @throws Exception
	 */
	@Test
	public void test05mutantDiagnonalUPAtRowZero() throws Exception {
		
		String request = "{\n" + 
				"	\"dna\": [\"AACCCCT\",\n" + 
				"	        \"CTCACTT\",\n" + 
				"	        \"ACCCTAT\",\n" + 
				"	        \"ACCTACC\",\n" + 
				"	        \"CAACTCC\",\n" + 
				"	        \"CAACAAT\",\n" + 
				"	        \"CAACAAT\"]\n" + 
				"}\n" + 
				""; 
		mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isOk());
		
	}

	/**
	 * Test - Human - Teste Diagonal UP - find only 1 sequence at row 0
	 * 
	 * @throws Exception
	 */
	@Test
	public void test06humanDiagnonalUPOneSequenceAtRowZero() throws Exception {
		
		String request = "{\n" + 
				"	\"dna\": [\"AATCCCA\",\n" + 
				"	        \"CTCACTT\",\n" + 
				"	        \"ACCCTAT\",\n" + 
				"	        \"ACCTACC\",\n" + 
				"	        \"CAATTCC\",\n" + 
				"	        \"CAACAAT\",\n" + 
				"	        \"CAACAAT\"]\n" + 
				"}"; 
		mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isForbidden());
		
	}

	/**
	 * Test - Mutant - Teste Diagonal DOWN respective last lines
	 * 
	 * @throws Exception
	 */
	@Test
	public void test07mutantDiagnonalDOWNRespectiveLastLine() throws Exception {
		
		String request = "{\n" + 
				"	\"dna\": [\"AATACCA\",\n" + 
				"	        \"CTCAATT\",\n" + 
				"	        \"ACCCTAT\",\n" + 
				"	        \"ACCTACA\",\n" + 
				"	        \"CAATTCC\",\n" + 
				"	        \"CAACAAT\",\n" + 
				"	        \"CAAAAAT\"]\n" + 
				"}\n" + 
				""; 
		mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isOk());
		
	}

	/**
	 * Test - Mutante - Test Horizontal
	 * 
	 * @throws Exception
	 */
	@Test
	public void test08mutantHorizontal() throws Exception {
		
		String request = "{\n" + 
				"	\"dna\": [\"ACCCCCT\",\n" + 
				"	        \"CTCACTT\",\n" + 
				"	        \"ACCCTAT\",\n" + 
				"	        \"ACCTACC\",\n" + 
				"	        \"CAACTCC\",\n" + 
				"	        \"CAACAAT\",\n" + 
				"	        \"CAACCCC\"]\n" + 
				"}\n" + 
				""; 
		mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isOk());
		
	}

	/**
	 * Test - Mutante - Test Vertical
	 * 
	 * @throws Exception
	 */
	@Test
	public void test09mutantVertical() throws Exception {
		
		String request = "{\n" + 
				"	\"dna\": [\"ACCACCT\",\n" + 
				"	        \"CTCACTT\",\n" + 
				"	        \"ACCATAT\",\n" + 
				"	        \"AAAGACT\",\n" + 
				"	        \"CAACTCC\",\n" + 
				"	        \"CAACAAT\",\n" + 
				"	        \"CAACCCA\"]\n" + 
				"}\n" + 
				""; 
		mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isOk());
		
	}

	/**
	 * Test - Mutante - Test Vertical first and last column
	 * 
	 * @throws Exception
	 */
	@Test
	public void test10mutantVertical() throws Exception {
		
		String request = "{\n" + 
				"	\"dna\": [\"ACCACCT\",\n" + 
				"	        \"ATCACTT\",\n" + 
				"	        \"ACCATAC\",\n" + 
				"	        \"AAAGACG\",\n" + 
				"	        \"CCACTGG\",\n" + 
				"	        \"CATCAGG\",\n" + 
				"	        \"CAACCGG\"]\n" + 
				"}\n" + 
				""; 
		mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isOk());
		
	}

	/**
	 * Test - Mutant/Human - Status with 2 human and 6 mutant
	 * 
	 * @throws Exception
	 */
	@Test
	public void test11StatsMutantHuman() throws Exception {
		
		mockMvc.perform(
				get("/stats/")
					.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$.count_mutant_dna").value(6))
				.andExpect(jsonPath("$.count_human_dna").value(2))
				.andExpect(jsonPath("$.ratio").value(3));
		
		
	}

	/**
	 * Test - Human - Sequence less than 4
	 * 
	 * @throws Exception
	 */
	@Test
	public void test12HumanSequenceLessThan4() throws Exception {
		
		String request = "{\n"+ 
				"\"dna\": " +
				"        [\"ACC\",\n" + 
				"         \"CTC\",\n" + 
				"         \"ACG\"\n" + 
				"        ]\n" +
				"}"; 
		mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isForbidden())
			;
		
	}

	/**
	 * Test - Mutant - Test Diagonal UP/DOWN -Diagonal Zero )
	 * 
	 * @throws Exception
	 */
	@Test
	public void test13mutantDiagnonalZero() throws Exception {
		
		String request = "{\n" + 
				"	\"dna\": [\"AACCGCT\",\n" + 
				"	        \"CTCACTT\",\n" + 
				"	        \"ACCCCAT\",\n" + 
				"	        \"ACGCGCC\",\n" + 
				"	        \"CACATCC\",\n" + 
				"	        \"CCACAAT\",\n" + 
				"	        \"CAACAAT\"]\n" + 
				"}\n" + 
				""; 
		mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isOk());
		
	}

	/**
	 * Test - Mutant - Test Diagonal UP/DOWN -Diagonal Zero )
	 * 
	 * @throws Exception
	 */
	@Test
	public void test13mutantDiagnonalZeroDown() throws Exception {
		
		String request = "{\n" + 
				"	\"dna\": [\"AACCGCT\",\n" + 
				"	        \"CTCACTT\",\n" + 
				"	        \"ACGCTAT\",\n" + 
				"	        \"ACGGGCC\",\n" + 
				"	        \"CACAGCC\",\n" + 
				"	        \"CCACAGT\",\n" + 
				"	        \"TAACAAG\"]\n" + 
				"}\n" + 
				""; 
		mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isOk());
		
	}

	/**
	 * Test - Mutant - Olny Diagonal UP/DOWN - Non Diagonal Zero )
	 * 
	 * @throws Exception
	 */
	@Test
	public void test14mutantDiagnonalOnlyDown() throws Exception {
		
		String request = "{\n" + 
				"  \"dna\":[\"AACAGCT\",\n" + 
				"	        \"CTCAATT\",\n" + 
				"	        \"ACGCTAT\",\n" + 
				"	        \"ACGCGCA\",\n" + 
				"	        \"CACGGCC\",\n" + 
				"	        \"CCACGGT\",\n" + 
				"	        \"TAACAGG\"]\n" + 
				"}\n" + 
				""; 
		mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isOk());
		
	}

	/**
	 * Test - Mutant - Olny Diagonal UP/DOWN - Non Diagonal Zero )
	 * 
	 * @throws Exception
	 */
	@Test
	public void test15mutantDiagnonalOnlyuP() throws Exception {
		
		String request = "{\n" + 
				"  \"dna\":[\"ACATGCT\",\n" + 
				"	        \"CTTAGTT\",\n" + 
				"	        \"ATGCTAT\",\n" + 
				"	        \"TCCCGAC\",\n" + 
				"	        \"CACTGCC\",\n" + 
				"	        \"CCATCGT\",\n" + 
				"	        \"TAACACG\"]\n" + 
				"}\n" + 
				""; 
		mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isOk());
		
	}

	/**
	 * Invalid Request - The length of the DNA sequences must be the same size.
	 * 
	 * 
	 * @throws Exception
	 */
	@Test
	public void invalidRequestInconsistentLength() throws Exception {
		
		String request = "{\n" + 
				"	\"dna\": [\"CTXXXXT\",\n" + 
				"	        \"CTCACTT\",\n" + 
				"	        \"ACGCTAT\",\n" + 
				"	        \"ACCTACC\",\n" + 
				"	        \"CAATTCC\",\n" + 
				"	        \"CACCAAC\",\n" + 
				"	        \"CAACAAT\"]\n" + 
				"}\n" + 
				""; 
		ResultActions result = mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isBadRequest());
		String response = result.andReturn().getResponse().getContentAsString();
		response.contains("dna.sequence.inconsistent.length");
		response.contains("The length of the DNA sequences must be the same size");
		
	}

	/**
	 * Invalid Request - Invalid Nitrogenous base. The only valid characters are A,
	 * T, C e G.
	 * 
	 * 
	 * @throws Exception
	 */
	@Test
	public void invalidRequestInvalidNitrogenousBase() throws Exception {
		
		String request = "{\n" + 
				"	\"dna\": [\"ACCTATC\",\n" + 
				"	        \"CTCACTT\",\n" + 
				"	        \"ACGCTAT\",\n" + 
				"	        \"ACCTACC\",\n" + 
				"	        \"CAATTCC\",\n" + 
				"	        \"CC\",\n" + 
				"	        \"CAACAAT\"]\n" + 
				"}\n" + 
				""; 
		ResultActions result = mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isBadRequest());
		String response = result.andReturn().getResponse().getContentAsString();
		response.contains("dna.invalid.nitrogenous.base");
		response.contains("Invalid Nitrogenous base. The only valid characters are A, T, C e G.");
		response.contains("The only valid characters are A, T, C e G. Found invalida char in BCCTATC");
		
	}

	/**
	 * Invalid Request - Invalid Nitrogenous base. The only valid characters are A,
	 * T, C e G.
	 * 
	 * 
	 * @throws Exception
	 */
	@Test
	public void invalidRequestWithoutDNA() throws Exception {
		
		String request = "{ }";
		ResultActions result = mockMvc.perform(
				post("/mutant/")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.content(request.getBytes()))
				.andExpect(status().isBadRequest());
		String response = result.andReturn().getResponse().getContentAsString();
		response.contains("NotNull.DNASequence.dna");
		response.contains("DNA sequence is required");
		
		
	}

	@Test
	public void coverageDomain() {
		EstadoADN dnaStatus = new EstadoADN();
		// coverage only
		dnaStatus.setCantidadHumanos(0L);
		dnaStatus.setCantidadMutantes(0L);
		dnaStatus.setRatio(BigDecimal.ZERO);
		dnaStatus.setTotal(0L);
		System.out.println(dnaStatus);

		SecuenciaADN sequence = new SecuenciaADN();
		sequence.setDna(Arrays.asList("A"));
		sequence.getDna();
		System.out.println(sequence);

		ResultadoADN result = new ResultadoADN();
		result.setAdn(sequence);
		result.setEsMutante(false);
		result.getAdn();
		System.out.println(result);

		 

	}

}
