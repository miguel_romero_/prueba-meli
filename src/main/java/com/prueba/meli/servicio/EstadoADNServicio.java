package com.prueba.meli.servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.meli.entidad.EstadoADN;
import com.prueba.meli.repositorio.RepositorioEstadoADN;

@Service
public class EstadoADNServicio {

	@Autowired
	private RepositorioEstadoADN repositorio;
	
	
	/*
	 * llamado al repositorio para el estado de la cantidad de mutantes
	 * y humanos encontrados
	 */
	public EstadoADN obtenerEstadoActual() {
		return repositorio.getEstadoMutantes();
	}

}
