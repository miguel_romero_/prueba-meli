package com.prueba.meli.servicio;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.meli.configuracion.PropiedadesMutante;
import com.prueba.meli.detector.Detector;
import com.prueba.meli.entidad.ResultadoADN;
import com.prueba.meli.entidad.SecuenciaADN;
import com.prueba.meli.excepcion.PatronInvalidoExcepcion;
import com.prueba.meli.excepcion.TamanioInvalidoExcepcion;
import com.prueba.meli.repositorio.RepositorioResultadoADN;

@Service
public class MutanteServicio {

	private static final Pattern PATRON = Pattern.compile("[atcg]+", Pattern.CASE_INSENSITIVE);
	
	@Autowired
	private RepositorioResultadoADN repositorio;
	
	public boolean isMutant(SecuenciaADN secuencia) {
		
		boolean adnMutante = esADNMutante(secuencia);
		
		ResultadoADN resultado = new ResultadoADN();
		resultado.setAdn(secuencia);
		resultado.setEsMutante(adnMutante);

		this.repositorio.save(resultado);
		
		return adnMutante;
	}
	
	private boolean esADNMutante(SecuenciaADN secuencia) {
		
		if(secuencia.getDna().size() <= PropiedadesMutante.SECUENCIA_MUTANTE ){
			return false;
		}
		
		char[][] adn = cargarEstructuraADN(secuencia);
		Detector detector = new Detector(adn, PropiedadesMutante.CANTIDAD_SECUENCIAS_MUTANTE, 
				PropiedadesMutante.SECUENCIA_MUTANTE);
		
		return detector.isMutant();
	}
	 
	private char[][] cargarEstructuraADN(SecuenciaADN secuenciaAdn){
		int tamanioFila = secuenciaAdn.getDna().size();
		char[][] adn = new char[tamanioFila][tamanioFila];
		
		for(int i = 0; i < tamanioFila; i++) {
			String fila = secuenciaAdn.getDna().get(i);
			validarConsistenciaArreglo(tamanioFila, fila);
			adn[i] = fila.toUpperCase().toCharArray();
		}
		
		return adn;
	}
	
	private void validarConsistenciaArreglo(int tamanio, String fila) throws TamanioInvalidoExcepcion, PatronInvalidoExcepcion {
		if (fila.length() != tamanio) {
			throw new TamanioInvalidoExcepcion(tamanio, fila.length());
		} else if (!PATRON.matcher(fila).matches()) { 
			throw new PatronInvalidoExcepcion(fila);
		}
	}
}
