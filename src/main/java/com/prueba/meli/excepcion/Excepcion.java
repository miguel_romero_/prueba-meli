package com.prueba.meli.excepcion;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Excepcion extends RuntimeException{
	
	private String code;
	private HttpStatus statusCode;
	private String message;
	private String erroMessage;
	private Object[] argsMessage;
 
	public Excepcion(String error, HttpStatus status) {
		super(error);
		this.code = error;
		this.statusCode = status;
	}
 
	public Excepcion(String error, HttpStatus status, String errorMessage) {
		super(error);
		this.code = error;
		this.statusCode = status;
		this.erroMessage = errorMessage;
	}
}
