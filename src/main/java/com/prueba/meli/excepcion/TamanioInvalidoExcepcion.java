package com.prueba.meli.excepcion;

import org.springframework.http.HttpStatus;

public class TamanioInvalidoExcepcion extends Excepcion {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8197011898341285294L;

	private static final String ERROR = "adn.tamanio.incorrecto";
	
	
	public TamanioInvalidoExcepcion(int tamanio, int tamanioFila) {
		super(ERROR, HttpStatus.BAD_REQUEST);
		String msgError = "los tamanios deben ser los mismos " + tamanio + ", encontrado "
				+ tamanioFila;
		super.setErroMessage(msgError);
	}

}
