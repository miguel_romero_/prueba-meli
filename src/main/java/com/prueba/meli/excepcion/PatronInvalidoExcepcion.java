package com.prueba.meli.excepcion;

import org.springframework.http.HttpStatus;

public class PatronInvalidoExcepcion extends Excepcion {
 
	private static final long serialVersionUID = -5305199120453117787L;

	private static final String error = "adn.patron.invalido";
	
	public PatronInvalidoExcepcion(String fila) {
		super(error, HttpStatus.BAD_REQUEST,
				"Solo se permiten los caracteres A, T, C e G. encontrado" + fila);
	}

}
