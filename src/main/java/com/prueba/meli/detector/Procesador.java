package com.prueba.meli.detector;
  
/**
 * 
 * clase base de los procesadores, esta se encarga de buscar las secuencias 
 * alimenta estadisticas de mutantes encontrados y contiene la clase Coordenada
 * para ubicar los patrones en la matriz de datos
 *
 */
public abstract class Procesador {


	/**
	 * contexto que pasa entre los diferentes procesadores
	 * y almacena los datos encontrados en memoria 
	 */
	protected final Contexto contexto;

	/*
	 * constructor con el contexto
	 */
	public Procesador(Contexto contexto) {
		this.contexto = contexto;
	}
	
	protected abstract void buscarSecuencias();
	protected abstract void siguiente(Coordenada coordenada);
	protected abstract boolean haySiguiente(Coordenada coordenada, int secuenciaActual);
	
	/**
	 * 
	 * metodo el cual ejecuta la busqueda del patron en las coordenadas indicadas
	 * de la matriz de datos
	 */
	protected boolean encontrarSecuenciaMutante(Coordenada coordenada) {
		char actual = coordenada.adn[coordenada.fila][coordenada.columna];
		int secuencia = 1;
		
		while(haySiguiente(coordenada, secuencia)) {
			siguiente(coordenada);
			
			if (actual != coordenada.actualCaracter) {
				secuencia = 1;
				actual = coordenada.actualCaracter;
			} else if (++secuencia >= contexto.getSecuenciaMutante()) { 
				this.encontrado();

				if (encuentraMutante()) {
					return true;
				}
			} 
		}
		return false;
	}
	
	protected void encontrado() {
		contexto.setEncontrados(contexto.getEncontrados() + 1);

	}
	
	public boolean encuentraMutante() { 
		int count = contexto.getMutantesEncontrados();
		return contexto.getEncontrados() >= count;
	}
	
	/**
	 * 
	 * clase coordenada la cual permite obtener los datos de las filas y las columnas
	 * actualmente analizadas en la matriz de datos
	 * es la que guia a los procesadores por donde deben buscar
	 *
	 */
	
	public static class Coordenada {
		/*
		 * adn es la matriz de datos
		 */
		char adn[][];
		int subIndex;
		int fila;
		int columna;
		char ultimoCaracter;
		char actualCaracter;
		int index;
		int size;

		public Coordenada(char[][] adn, int subIndex, int fila, int columna) {
			super();
			this.adn = adn;
			this.index = adn.length - 1;
			this.size = adn.length;
			this.subIndex = subIndex;
			this.fila = fila;
			this.columna = columna;
			this.ultimoCaracter = adn[fila][columna];
		}
 
 
		public static Coordenada at(char[][] adn, int fila, int columna) {
			return new Coordenada(adn, 0, fila, columna);
		}

		public static Coordenada at(char[][] adn, int fila, int columna, int index) {
			return new Coordenada(adn, index, fila, columna);
		}
	}
}


