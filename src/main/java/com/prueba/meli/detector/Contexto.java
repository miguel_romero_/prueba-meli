package com.prueba.meli.detector;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


/**
 * contexto que pasa entre los diferentes procesadores
 * y almacena los datos encontrados en memoria 
 */


@Getter
@Setter
@Builder
public class Contexto {
	char[][] adn;
	private int secuenciaMutante;
	private int mutantesEncontrados;
	private int encontrados;
}
