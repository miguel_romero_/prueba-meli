package com.prueba.meli.detector;

public class Diagonal extends Procesador{

	public Diagonal(Contexto contexto) {
		super(contexto); 
	}
	/**
	 * metodo que busca las secuencias segun lo parametrizado con las coordenadas
	 */
	@Override
	protected void buscarSecuencias() {  
		
		char[][] adn = contexto.getAdn();

		boolean encontrado = encontrarSecuenciaMutante(Coordenada.at(adn, 0, 0));
		if (encontrado) {
			return;
		}
		for (int fila = 1; fila <= adn.length - contexto.getSecuenciaMutante(); fila++) {
			encontrado = encontrarSecuenciaMutante(Coordenada.at(adn, fila, 0, fila))
					|| encontrarSecuenciaMutante(Coordenada.at(adn, 0, fila, fila));
			if (encontrado) {
				break;
			}
		}

	}
	/*
	 * metodo que pasa a la siguiente posicion de la coordenada enviada
	 */
	@Override
	protected void siguiente(Coordenada coordenada) {
		coordenada.fila++;
		coordenada.columna++;
		coordenada.subIndex++;
		coordenada.ultimoCaracter = coordenada.actualCaracter;
		coordenada.actualCaracter = coordenada.adn[coordenada.fila][coordenada.columna];
	}
	/*
	 * metodo que valida si existe una posicion siguiente a la coordenada enviada
	 */
	@Override
	protected boolean haySiguiente(Coordenada coordenada, int secuenciaActual) {
		int subIndex = coordenada.subIndex;
		int disponible = coordenada.size - subIndex;
		return disponible + secuenciaActual >= contexto.getSecuenciaMutante()
				&& Math.max(coordenada.columna, coordenada.fila) + 1 < coordenada.size;
	}

}
