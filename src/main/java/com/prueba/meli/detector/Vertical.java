package com.prueba.meli.detector;
  
public class Vertical extends Procesador{ 
	
	public Vertical(Contexto contexto) {
		super(contexto);
	}
	/**
	 * metodo que busca las secuencias segun lo parametrizado con las coordenadas
	 */
	@Override
	protected void buscarSecuencias() {
		
		char[][] adn = contexto.getAdn(); 
		for (int columna = 0; columna < adn.length; columna++) {
			boolean encontrado = encontrarSecuenciaMutante(Coordenada.at(adn, 0, columna));
			if (encontrado) {
				break;
			}
		}
	}
	/*
	 * metodo que pasa a la siguiente posicion de la coordenada enviada
	 */
	@Override
	protected void siguiente(Coordenada coordinate) {
		coordinate.fila++;
		coordinate.subIndex++;
		coordinate.ultimoCaracter = coordinate.actualCaracter;
		coordinate.actualCaracter = coordinate.adn[coordinate.fila][coordinate.columna];
	}
	/*
	 * metodo que valida si existe una posicion siguiente a la coordenada enviada
	 */
	@Override
	protected boolean haySiguiente(Coordenada coordinate, int actualSequence) {
		return coordinate.fila + 1 <= coordinate.index;
	}
}
