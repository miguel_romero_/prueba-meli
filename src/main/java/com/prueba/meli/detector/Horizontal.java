package com.prueba.meli.detector;

public class Horizontal extends Procesador{

	/*
	 * constructor con el contexto compartido con los demas procesadores
	 */
	public Horizontal(Contexto contexto) {
		super(contexto);
	}

	
	/**
	 * metodo que busca las secuencias segun lo parametrizado con las coordenadas
	 */
	@Override
	protected void buscarSecuencias() {
		char[][] adn = contexto.getAdn();
		for (int fila = 0; fila < adn.length; fila++) {
			boolean encontrado = encontrarSecuenciaMutante(Coordenada.at(adn, fila, 0));
			if (encontrado) {
				break;
			}
		}
	}

	/*
	 * metodo que pasa a la siguiente posicion de la coordenada enviada
	 */
	@Override
	protected void siguiente(Coordenada coordenada) {
		coordenada.columna++;
		coordenada.subIndex++;
		coordenada.ultimoCaracter = coordenada.actualCaracter;
		coordenada.actualCaracter = coordenada.adn[coordenada.fila][coordenada.columna];
	}

	/*
	 * metodo que valida si existe una posicion siguiente a la coordenada enviada
	 */
	@Override
	protected boolean haySiguiente(Coordenada coordenada, int secuenciaActual) {
		return  coordenada.columna + 1 <=  coordenada.index;
	}
}
