package com.prueba.meli.detector;

import java.util.LinkedList;
import java.util.List;

/**
 * 
 * clase que orquesta la deteccion de los procesadores creados
 * para hallar si es o no un mutante
 *
 */

public class Detector {

	/*
	 * contexto para almacenar mutantes encontrados entre los distintos
	 * procesadores
	 */
	private Contexto contexto;
	List<Procesador> procesadores = new LinkedList<>();
	
	/*
	 * Constructor del detector para la inicializacion del contexto
	 * con los parametros deseados
	 */
	
	public Detector(char[][] adn, int mutantesEncontrados, int secuenciaMutante) {
		this.contexto = Contexto.builder()
				.adn(adn)
				.mutantesEncontrados(mutantesEncontrados)
				.secuenciaMutante(secuenciaMutante)
				.build();
		
		cargarProcesadores();
	}

	/*
	 * cargue de los procesadores que van a encontrar los mutantes en las diferentes 
	 * direcciones de analisis de la matriz de datos
	 */
	
	private void cargarProcesadores() {
		
		/*
		 * carga de procesador que busca en sentido horizontal
		 */
		Procesador horizontal = new Horizontal(contexto);
		registrarProcesador(horizontal);

		/*
		 * carga de procesador que busca en sentido vertical
		 */
		Procesador vertical = new Vertical(contexto);
		registrarProcesador(vertical);

		/*
		 * carga de procesador que busca en sentido diagonal
		 */
		Procesador diagonal = new Diagonal(contexto);
		registrarProcesador(diagonal);

		/*
		 * carga de procesador que busca en sentido diagonal inverso
		 */
		Procesador diagonalInverso = new DiagonalInverso(contexto);
		registrarProcesador(diagonalInverso);
		
	}
	
	/*
	 * alimentacion de la lista de procesadores
	 */
	private void registrarProcesador(Procesador procesador) {
		procesadores.add(procesador);
	}
	
	/*
	 * Metodo que ejecuta cada uno de los procesadores buscando secuencias y 
	 * alimenta metricas del mismo si encuentra un mutante
	 * 
	 */
	public boolean isMutant() {
		for (Procesador procesador : procesadores) {
			procesador.buscarSecuencias();
			if (procesador.encuentraMutante()) {
				break;
			}
		}
		return contexto.getEncontrados() >= contexto.getMutantesEncontrados();
	}
}
