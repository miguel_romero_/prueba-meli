package com.prueba.meli.entidad;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Document(collection = "dna_result")
@Getter
@Setter
@EqualsAndHashCode
public class ResultadoADN {
	
	@Id
	private SecuenciaADN adn;

	@Indexed(name = "es_mutante")
	private boolean esMutante;
}
