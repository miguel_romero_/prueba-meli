package com.prueba.meli.entidad;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class SecuenciaADN {
	
	@NotNull
	private List<String> dna;
}
