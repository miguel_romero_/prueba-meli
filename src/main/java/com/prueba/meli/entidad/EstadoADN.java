package com.prueba.meli.entidad;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class EstadoADN {
	
	@JsonProperty("count_mutant_dna")
	private Long cantidadMutantes = 0L;
	@JsonProperty("count_human_dna")
	private Long cantidadHumanos = 0L;
	private BigDecimal ratio;
	@JsonIgnore
	private Long total = 0L;
}
