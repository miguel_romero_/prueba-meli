package com.prueba.meli.controlador;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.meli.entidad.SecuenciaADN;
import com.prueba.meli.servicio.MutanteServicio;

@RestController
@RequestMapping("/mutant")
public class MutanteControlador {

	@Autowired
	private MutanteServicio servicio;
	
	@PostMapping("/")
	public ResponseEntity<Void> isMutant(@RequestBody @Valid SecuenciaADN secuencia) { 

		boolean isMutant = servicio.isMutant(secuencia);
		if (isMutant) {
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		}
	}
}
