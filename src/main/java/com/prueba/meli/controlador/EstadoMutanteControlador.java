package com.prueba.meli.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.meli.entidad.EstadoADN;
import com.prueba.meli.servicio.EstadoADNServicio;

/**
 *  
 *	
 *	controlador para ver el estatus de los mutantes encontrados
 *
 */

@RestController
@RequestMapping("/stats")
public class EstadoMutanteControlador {

	/*
	 * servicio que encuentra los mutantes
	 */
	@Autowired
	private EstadoADNServicio servicio;
	
	/*
	 * endpoint para obtener el estado de la cantidad de 
	 * mutantes y humanos encontrados
	 */ 
	@GetMapping
	public ResponseEntity<EstadoADN> estadosADNMutante(){
		return ResponseEntity.ok(servicio.obtenerEstadoActual());
	}
}
