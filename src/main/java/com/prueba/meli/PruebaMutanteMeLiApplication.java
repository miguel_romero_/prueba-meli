package com.prueba.meli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication; 

@SpringBootApplication
public class PruebaMutanteMeLiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaMutanteMeLiApplication.class, args);
	}

}
