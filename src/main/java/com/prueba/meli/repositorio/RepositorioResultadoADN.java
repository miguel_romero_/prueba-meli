package com.prueba.meli.repositorio;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.prueba.meli.entidad.ResultadoADN;

@Repository
public interface RepositorioResultadoADN extends MongoRepository<ResultadoADN, ResultadoADN>{

}
