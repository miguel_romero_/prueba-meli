package com.prueba.meli.repositorio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.ComparisonOperators;
import org.springframework.data.mongodb.core.aggregation.ConditionalOperators;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Repository;

import com.prueba.meli.entidad.EstadoADN;
import com.prueba.meli.entidad.ResultadoADN;

@Repository
public class RepositorioEstadoADN {
	
	@Autowired
	private transient MongoTemplate template;
	
	
	/*
	 * llama las funciones que calculan la cantidad y el ratio 
	 * de mutantes y humanos encontrados 
	 */
	
	public EstadoADN getEstadoMutantes(){ 

		EstadoADN estado = calcularResultados();
		BigDecimal ratio = calcularRatio(estado);
		estado.setRatio(ratio);

		return estado; 
	}
	
	
	private BigDecimal calcularRatio(EstadoADN status) {
		/*
		 * ratio inicializa en cero
		 */
		BigDecimal ratio = BigDecimal.ZERO;

		/*
		 * revisa si la cantidad de mutantes es 0
		 */
		if (status.getCantidadMutantes() != 0) {
			/*
			 * revisa la cantidad de humanos si es cero se ratio uno para evitar divicion por cero
			 */
			if (status.getCantidadHumanos() == 0) {
				ratio = BigDecimal.ONE;
			} else {
				/*
				 * si humanos es diferente de cero divide la cantidad de mutantes en la cantidad
				 * de humanos y redondea hacia arriba dado el caso que sea un decimal
				 */
				BigDecimal mutantes = BigDecimal.valueOf(status.getCantidadMutantes());
				BigDecimal humanos = BigDecimal.valueOf(status.getCantidadHumanos());
				ratio = mutantes.divide(humanos, 2, RoundingMode.HALF_UP);
			}
		}
		return ratio;
	}


	private EstadoADN calcularResultados() {

		Aggregation aggregation = Aggregation.newAggregation(
		group()
			/*
			 * agrupa la cantidad de humanos y de mutantes segun lo encontado en 
			 * la base de datos por el valor de la variable esMutante y suma los resultados
			 */
			.sum(ConditionalOperators
					.when(ComparisonOperators.valueOf("esMutante").equalToValue(true))
					.then(1).otherwise(0)).as("cantidadMutantes")
			.sum(ConditionalOperators
					.when(ComparisonOperators.valueOf("esMutante").equalToValue(false))
					.then(1).otherwise(0)).as("cantidadHumanos")
			.count().as("total")
		);

		/*
		 * almacena la suma anterior en un espacio para mongo
		 */
		AggregationResults<EstadoADN> result = 
				this.template.aggregate(aggregation, ResultadoADN.class, EstadoADN.class);

		/*
		 * retorna los datos consultados
		 */
		
		EstadoADN status = result.getUniqueMappedResult();
		return status == null ? new EstadoADN() : status;
	}
	
}
