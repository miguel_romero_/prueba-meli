package com.prueba.meli.configuracion;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
 
import lombok.Getter; 

@Configuration
@ConfigurationProperties(prefix = "mutante", ignoreUnknownFields = false)  
public class PropiedadesMutante {

	public static final int SECUENCIA_MUTANTE = 4;
	public static final int CANTIDAD_SECUENCIAS_MUTANTE = 2;

}
